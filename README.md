# Web app

This is the web application for Nuitch built with Django 2.2 on python 3.

## Requirements
This project has the following requirements:

* python 3
* pip 3
* postgreSQL
* ffmpeg

## Working with git
**BEFORE DOING ANY COMMITS please read these instructions**.

PLEASE work with branches. Every feature being developed in the project at any moment has a branch associated with it. Please *switch to the branch* that correspond to the feature you're working with.

You can check out **all the branches** in the project with the command `git branch --all`.

You can check your **current branch** with the command `git branch -l`.

### Switching to new branch
The first time you switch to a branch you need to use the following command:
```sh
git checkout -b <local-branch-name> origin/<remote-branch-name>
```

You have to replace `<local-branch-name>` with the name of the branch you're gonna be working locally and `<remote-branch-name>` with the name of the remote branch.

For example:
```sh
git checkout -b video-service origin/video-service
Branch 'video-service' set up to track remote branch 'video-service' from 'origin'.
Switched to a new branch 'video-service'
```

### Switching to an already existing branch
If you've already created a local branch you only need to use the following command:
```sh
git checkout <local-branch-name>
```

For example:
```sh
git checkout master
```

### Updating a branch
You can update a branch with the changes from master with the commands:
```sh
git fetch
git rebase origin/master
```

This has the effect of adding all the new changes from master to your current branch.

### Pushing changes on a branch
You can push your changes on a branch with the command
```sh
git push origin <remote-branch-name>
```

## Getting started

Setup the virtual environment.
```sh
virtualenv venv
source venv/bin/activate
pip install -r requirements.txt
```

Open postgres to create a database and a user.
```sh
sudo systemctl start postgresql
sudo su - postgres
psql
```

If you have problems starting the database run the command `sudo /usr/bin/postgresql-setup --initdb` to initialize postgres.

Create the database and the user.
```sql
CREATE DATABASE nuitch;
CREATE USER nuitch_user WITH ENCRYPTED PASSWORD 'password';
GRANT ALL PRIVILEGES ON DATABASE nuitch TO nuitch_user;
```

To allow authenticated connections to the database you have to edit the file `/var/lib/pgsql/data/pg_hba.conf` to look like this.
```
# TYPE  DATABASE        USER            ADDRESS                 METHOD

# "local" is for Unix domain socket connections only
local   all             all                                     peer
# IPv4 local connections:
host    all             all             127.0.0.1/32            md5
# IPv6 local connections:
host    all             all             ::1/128                 ident
# Allow replication connections from localhost, by a user with the
# replication privilege.
local   replication     all                                     peer
host    replication     all             127.0.0.1/32            md5
host    replication     all             ::1/128                 ident
```
You need to restart postgres after that with `sudo systemctl restart postgresql`.

Create an `.env` file and **configure the database credentials**.
```sh
cp .env.example .env
```

Run the migrations
```sh
python manage.py migrate
```

## Load initial data
**You need to load initial data to database**. For that, running this command:

```sh
python manage.py loaddata initial-data.json
```

The initial data is saved in [initial-data.json](nuitch_site/fixtures/initial-data.json).

With this action, you have four types of users (those types are possible because initial data create four groups with necessary permissions).

+ superuser (django default superuser)
+ superadministrator (user with all permissions)
+ administrator (user with permission for community manager)
+ user (normal user) (when a new user sign up, it is added to user group)

This action add four users too. The usernames are above too. Ask for passwords.
