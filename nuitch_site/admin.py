from django.contrib import admin
from django.contrib.auth.models import User
from . import models

class UserAdmin(admin.ModelAdmin):
    search_fields = ['id','username','email']
    list_display = ['id','username','email']

class MovieAdmin(admin.ModelAdmin):
    search_fields = ['id','title','studio','director','length','video_file']
    list_display = ['id','title','studio','director','length','video_file']

class MovieMetadataAdmin(admin.ModelAdmin):
    list_display = ['movie','views_counter','rating','hidden','created_at']

class RatingAdmin(admin.ModelAdmin):
    list_display = ['id','user','movie','rating','created_at']

class ReviewAdmin(admin.ModelAdmin):
    list_display = ['id','user','movie','text','created_at']

class FavoriteAdmin(admin.ModelAdmin):
    list_display = ['id','user','movie','created_at']

class HistoryAdmin(admin.ModelAdmin):
    list_display = ['id','user','movie','seconds','created_at']

class UserDetailsAdmin(admin.ModelAdmin):
    list_display = ['user','name','last_name','created_at']

class SuspensionAdmin(admin.ModelAdmin):
    list_display = ['id','user','complete','description','start_date','end_date']

# Register your models here.
admin.site.register(models.Movie, MovieAdmin)
admin.site.register(models.MovieMetadata, MovieMetadataAdmin)
admin.site.register(models.Rating, RatingAdmin)
admin.site.register(models.Review, ReviewAdmin)
admin.site.register(models.Favorite, FavoriteAdmin)
admin.site.register(models.History, HistoryAdmin)
admin.site.register(models.UserDetails, UserDetailsAdmin)
admin.site.register(models.Suspension, SuspensionAdmin)
admin.site.unregister(User)
admin.site.register(User, UserAdmin)
