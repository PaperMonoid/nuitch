from django.urls import path
from nuitch_site.views import video_page, home_page, user_panel

urlpatterns = [
    path("", home_page.index, name="home_page"),
    path("user", user_panel.index, name="user_panel"),
    path("user/delete", user_panel.delete_account, name="user_delete"),
    path("user/favorite", user_panel.favorite, name="user_favorite"),
    path(
        "user/favorite/delete/<int:id>",
        user_panel.delete_favorite,
        name="user_delete_favorite",
    ),
    path("user/history", user_panel.history, name="user_history"),
    path(
        "user/history/delete/<int:id>",
        user_panel.delete_history,
        name="user_delete_history",
    ),
    path("watch/<int:id>", video_page.watch_video, name="watch_video"),
    path("rate/list/", video_page.rate_list, name="rate_list"),
    path("rate/update/", video_page.rate_add_or_update, name="rate_update"),
    path("favorite/add/", video_page.favorite_add, name="favorite_add"),
    path("favorite/delete/", video_page.favorite_delete, name="favorite_delete"),
    path("review/list/", video_page.review_list, name="review_list"),
    path("review/update/", video_page.review_add_or_update, name="review_update"),
    path("review/delete/", video_page.review_delete, name="review_delete"),
    path("history/update/", video_page.history_update, name="history_update"),
]
