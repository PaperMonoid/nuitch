# Generated by Django 2.2.6 on 2019-11-18 19:50

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('nuitch_site', '0003_auto_20191118_1934'),
    ]

    operations = [
        migrations.AlterField(
            model_name='movie',
            name='director',
            field=models.CharField(default='No director', max_length=64),
        ),
        migrations.AlterField(
            model_name='movie',
            name='studio',
            field=models.CharField(default='No studio', max_length=256),
        ),
    ]
