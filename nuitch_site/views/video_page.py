from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required

# API
from rest_framework.authentication import TokenAuthentication
from rest_framework.authtoken.models import Token
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.decorators import (
    api_view,
    authentication_classes,
    permission_classes,
)
from rest_framework import serializers

# models
from django.contrib.auth.models import User
from nuitch_site.models import (
    Movie,
    MovieMetadata,
    Favorite,
    Rating,
    Review,
    History,
    UserDetails,
)


###############
#### VIEWS ####
###############


@login_required()
def watch_video(request, id):
    if request.GET is not None:
        context = {}
        movie = Movie.objects.get(id=id)
        user = User.objects.get(id=request.user.id)

        history = History.objects.filter(movie=movie).filter(user=user)

        if not history.exists():
            create_history(movie.id, user.id)

        history = History.objects.filter(movie=movie).filter(user=user)
        context["history"] = history.first()

        movie_metadata = MovieMetadata.objects.filter(movie=movie)

        context["movie"] = movie
        context["user"] = user
        context["movie_metadata"] = movie_metadata.first()

        if movie_metadata.first().hidden:
            return redirect("home_page")

        is_favorite = False
        favorite = Favorite.objects.filter(movie=movie).filter(user=user)
        if favorite.exists():
            is_favorite = True
        context["is_favorite"] = is_favorite

        related_metadatas = MovieMetadata.objects.filter(hidden=False)[:5]
        related = []
        for related_metadata in related_metadatas:
            related.append(related_metadata.movie)
        context["related"] = related

        is_user_review = False
        user_review = Review.objects.filter(movie=movie).filter(user=user)
        if user_review.exists():
            is_user_review = True
            context["user_review"] = user_review.first()
        else:
            context["user_review"] = {}

        reviews = Review.objects.all()[:5]
        context["reviews"] = reviews

        token = Token.objects.get_or_create(user=request.user)[0]
        context["token"] = token

        increment_movie_counter(id)

        return render(request, "nuitch_site/video_page/watch_video.html", context)


#####################
#### SERIALIZERS ####
#####################


class UserSerializer(serializers.ModelSerializer):
    name = serializers.SerializerMethodField()
    last_name = serializers.SerializerMethodField()

    def get_name(self, instance):
        user_details = UserDetails.objects.filter(user=instance)
        if user_details.exists():
            user_details = user_details.first()
            return user_details.name
        else:
            return ""

    def get_last_name(self, instance):
        user_details = UserDetails.objects.filter(user=instance)
        if user_details.exists():
            user_details = user_details.first()
            return user_details.last_name
        else:
            return ""

    class Meta:
        model = User
        fields = ["id", "username", "name", "last_name"]


class MovieSerializer(serializers.ModelSerializer):
    class Meta:
        model = Movie
        fields = ["id", "title"]


class ReviewSerializer(serializers.ModelSerializer):
    user = UserSerializer()
    movie = MovieSerializer()

    class Meta:
        model = Review
        fields = ["id", "user", "movie", "text", "updated_at"]


class RatingSerializer(serializers.ModelSerializer):
    user = UserSerializer()
    movie = MovieSerializer()

    class Meta:
        model = Rating
        fields = ["id", "user", "movie", "rating", "updated_at"]


###################
#### API VIEWS ####
###################


@api_view(["POST"])
@authentication_classes([TokenAuthentication])
@permission_classes([IsAuthenticated])
def rate_add_or_update(request):
    id_user = request.data.get("id_user")
    id_movie = request.data.get("id_movie", -1)
    rate = request.data.get("rate", -1)
    create_or_update_rate(id_movie, id_user, rate)
    update_movie_rating(id_movie)
    return Response(200)


@api_view(["GET"])
@authentication_classes([TokenAuthentication])
@permission_classes([IsAuthenticated])
def rate_list(request):
    id_movie = request.query_params.get("id_movie", -1)
    id_user = request.query_params.get("id_user", -1)
    rate = list_rate(id_movie, id_user)
    if rate.exists():
        rate = rate.first()
    else:
        movie = Movie.objects.get(id=id_movie)
        user = User.objects.get(id=id_user)
        rate = Rating()
        rate.user = user
        rate.movie = movie
        rate.rating = 0
    serializer = RatingSerializer(rate)
    return Response(serializer.data)


@api_view(["POST"])
@authentication_classes([TokenAuthentication])
@permission_classes([IsAuthenticated])
def favorite_add(request):
    id_user = request.data.get("id_user")
    id_movie = request.data.get("id_movie")
    create_favorite(id_movie, id_user)
    return Response(200)


@api_view(["POST"])
@authentication_classes([TokenAuthentication])
@permission_classes([IsAuthenticated])
def favorite_delete(request):
    id_user = request.data.get("id_user")
    id_movie = request.data.get("id_movie")
    delete_favorite(id_movie, id_user)
    return Response(200)


@api_view(["GET"])
@authentication_classes([TokenAuthentication])
@permission_classes([IsAuthenticated])
def review_list(request):
    id_movie = request.query_params.get("id_movie", -1)
    reviews = list_review(id_movie)
    serializer = ReviewSerializer(reviews, many=True)
    return Response(serializer.data)


@api_view(["POST"])
@authentication_classes([TokenAuthentication])
@permission_classes([IsAuthenticated])
def review_add_or_update(request):
    id_user = request.data.get("id_user")
    id_movie = request.data.get("id_movie", -1)
    text = request.data.get("text", -1)
    if text == -1 and validate_review(text):
        raise ValueError("Invalid review.")
    create_or_update_review(id_movie, id_user, text)
    return Response(200)


@api_view(["POST"])
@authentication_classes([TokenAuthentication])
@permission_classes([IsAuthenticated])
def review_delete(request):
    id_review = request.data.get("id_review", -1)
    delete_review(id_review)
    return Response(200)


@api_view(["POST"])
@authentication_classes([TokenAuthentication])
@permission_classes([IsAuthenticated])
def history_update(request):
    id_history = request.data.get("id_history", -1)
    seconds = request.data.get("seconds", -1)
    update_history(id_history, seconds)
    return Response(200)


def validate_review(text):
    if text.replace(" ", "") != "":
        return False
    else:
        return True


#################
#### METHODS ####
#################


def create_or_update_rate(id_movie, id_user, rate):
    user_rate = Rating.objects.filter(movie=id_movie).filter(user=id_user)

    if user_rate.exists():
        if rate >= 1 and rate <= 5:
            user_rate = user_rate.first()
            user_rate.rating = rate
            user_rate.save()
    else:
        movie = Movie.objects.get(id=id_movie)
        user = User.objects.get(id=id_user)
        if rate >= 1 and rate <= 5:
            user_rate = Rating()
            user_rate.movie = movie
            user_rate.user = user
            user_rate.rating = rate
            user_rate.save()


def list_rate(id_movie, id_user):
    return Rating.objects.filter(movie=id_movie).filter(user=id_user)


def increment_movie_counter(id_movie, quantity=1):
    movie = Movie.objects.get(id=id_movie)
    movie_metadata = MovieMetadata.objects.get(movie=movie)
    movie_metadata.views_counter = movie_metadata.views_counter + quantity
    movie_metadata.save()


def update_movie_rating(id_movie):
    movie = Movie.objects.get(id=id_movie)

    rate = 0
    movie_ratings = list(Rating.objects.filter(movie=movie))
    for movie_rating in movie_ratings:
        rate += movie_rating.rating
    rate /= len(movie_ratings)

    movie_metadata = MovieMetadata.objects.get(movie=movie)
    movie_metadata.rating = rate
    movie_metadata.save()


def create_favorite(id_movie, id_user):
    movie = Movie.objects.get(id=id_movie)
    user = User.objects.get(id=id_user)

    favorite = Favorite()
    favorite.user = user
    favorite.movie = movie
    favorite.save()


def delete_favorite(id_movie, id_user):
    favorite = Favorite.objects.filter(movie=id_movie).filter(user=id_user).first()
    favorite.delete()


def list_review(id_movie):
    return Review.objects.filter(movie=id_movie)


def create_or_update_review(id_movie, id_user, text):
    review = Review.objects.filter(movie=id_movie).filter(user=id_user)

    if review.exists():
        review = review.first()
        review.text = text
        review.save()
    else:
        movie = Movie.objects.get(id=id_movie)
        user = User.objects.get(id=id_user)

        review = Review()
        review.movie = movie
        review.user = user
        review.text = text
        review.save()


def delete_review(id_review):
    review = Review.objects.get(id=id_review)
    review.delete()


def create_history(id_movie, id_user, seconds=0):
    movie = Movie.objects.get(id=id_movie)
    user = User.objects.get(id=id_user)

    history = History()
    history.movie = movie
    history.user = user
    history.seconds = seconds
    history.save()


def update_history(id_history, seconds=0):
    history = History.objects.get(id=id_history)
    history.seconds = seconds
    history.save()
