from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from nuitch_site.models import Movie, MovieMetadata

# Create your views here.
@login_required()
def index(request):
    context = {}

    context["search"] = False
    movies_metadata = MovieMetadata.objects.filter(hidden=False).order_by(
        "movie__title"
    )

    top_rated = MovieMetadata.objects.filter(hidden=False).order_by("-rating")
    top_rated = top_rated[:5]
    top_views = MovieMetadata.objects.filter(hidden=False).order_by("-views_counter")
    top_views = top_views[:5]

    if request.POST:
        movies_metadata = movies_metadata.filter(
            movie__title__contains=request.POST.get("search")
        )
        context["search"] = True

    context["movies"] = movies_metadata
    context["top_rated"] = top_rated
    context["top_views"] = top_views

    return render(request, "nuitch_site/home_page/index.html", context)
