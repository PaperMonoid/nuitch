from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.contrib.auth import logout
from django.contrib.auth.decorators import login_required
from rest_framework.authentication import TokenAuthentication
from rest_framework.authtoken.models import Token
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.decorators import (
    api_view,
    authentication_classes,
    permission_classes,
)
from nuitch_site.models import History, Favorite, Movie, MovieMetadata, UserDetails


@login_required()
def index(request):
    details = UserDetails.objects.filter(user=request.user)

    if details.exists():
        details = details.first()
    else:
        details = UserDetails()
        details.user = request.user
        details.save()

    context = {"user": request.user, "details": details, "message": None}
    if request.POST:
        details.name = request.POST.get("name")
        details.last_name = request.POST.get("last_name")
        details.address = request.POST.get("address")
        details.phone = request.POST.get("phone")
        details.save()
        context["message"] = "All changes were saved!"

    return render(request, "nuitch_site/user_panel/user_details.html", context)


@login_required()
def history(request):
    history = History.objects.filter(user=request.user)
    history_not_hidden = []
    for item in history:
        metadata = MovieMetadata.objects.get(movie=item.movie)
        if not metadata.hidden:
            history_not_hidden.append(item)
    token = Token.objects.get_or_create(user=request.user)[0]
    context = {"history": history_not_hidden, "token": token}
    return render(request, "nuitch_site/user_panel/history.html", context)


@login_required()
def favorite(request):
    favorites = Favorite.objects.filter(user=request.user)
    favorites_not_hidden = []
    for item in favorites:
        metadata = MovieMetadata.objects.get(movie=item.movie)
        if not metadata.hidden:
            favorites_not_hidden.append(item)
    token = Token.objects.get_or_create(user=request.user)[0]
    context = {"favorites": favorites_not_hidden, "token": token}
    return render(request, "nuitch_site/user_panel/favorites.html", context)


@api_view(["POST"])
@authentication_classes([TokenAuthentication])
@permission_classes([IsAuthenticated])
def delete_history(request, id):
    history = History.objects.filter(id=id)
    if history.exists():
        history.first().delete()
    return Response(200)


@api_view(["POST"])
@authentication_classes([TokenAuthentication])
@permission_classes([IsAuthenticated])
def delete_favorite(request, id):
    favorite = Favorite.objects.filter(id=id)
    if favorite.exists():
        favorite.first().delete()
    return Response(200)


@login_required()
def delete_account(request):
    user = request.user
    logout(request)
    user.delete()
    return redirect("login")
