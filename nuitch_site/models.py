from django.conf import settings
from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save, pre_delete, post_delete
from django.dispatch import receiver
from django.utils.timezone import now
from django.core.files import File
import uuid
import os
import shutil
import ffmpeg_streaming
from ffmpeg_streaming import Representation


def get_video_file_path(instance, filename):
    ext = filename.split(".")[-1]
    filename = "%s/manifest.%s" % (uuid.uuid4(), ext)
    return os.path.join("videos", filename)


def get_image_file_path(instance, filename):
    ext = filename.split(".")[-1]
    filename = "%s.%s" % (uuid.uuid4(), ext)
    return os.path.join("images", filename)


class Movie(models.Model):
    title = models.CharField(max_length=256, default="No title")
    studio = models.CharField(max_length=256, default="No studio")
    director = models.CharField(max_length=64, default="No director")
    length = models.IntegerField(default=0)
    description = models.CharField(max_length=1024, default="No description")
    image = models.ImageField(
        upload_to=get_image_file_path,
        height_field="image_heigth",
        width_field="image_width",
    )
    image_heigth = models.IntegerField(default=0, editable=False)
    image_width = models.IntegerField(default=0, editable=False)
    video_file = models.FileField(upload_to=get_video_file_path)
    created_at = models.DateTimeField(default=now, editable=False)
    updated_at = models.DateTimeField(default=now, editable=False)

    def __str__(self):
        return "Id: %s | Title: %s | Director: %s" % (
            self.id,
            self.title,
            self.director,
        )


class MovieMetadata(models.Model):
    movie = models.OneToOneField(Movie, on_delete=models.SET_NULL, null=True)
    views_counter = models.IntegerField(default=0)
    rating = models.FloatField(default=0.0)
    hidden = models.BooleanField(default=True)
    created_at = models.DateTimeField(default=now, editable=False)
    updated_at = models.DateTimeField(default=now, editable=False)

    def __str__(self):
        return "Movie Id: %s | Title: %s | Director: %s" % (
            self.movie.id,
            self.movie.title,
            self.movie.director,
        )


@receiver(pre_delete, sender=MovieMetadata)
def delete_movie(sender, instance, **kwargs):
    """ Trigger body """
    # delete Movie after MovieMetadata is deleted, but is not posible
    if instance.movie is None:
        pass
    else:
        movie = Movie.objects.get(id=instance.movie.id)
        instance.movie = None
        instance.save()
        movie.delete()


@receiver(pre_delete, sender=Movie)
def delete_video_file_and_metadata(sender, instance, **kwargs):
    """ Trigger body """
    # delete file when before movie is deleted
    instance.video_file.delete()
    movie_metadata = MovieMetadata.objects.get(movie=instance)
    if movie_metadata is None:
        pass
    else:
        movie_metadata.movie = None
        movie_metadata.save()
        movie_metadata.delete()


@receiver(post_save, sender=Movie)
def create_movie_metadata(sender, instance, **kwargs):
    """ Trigger body """
    try:
        movie_metadata = None
        movie_metadata = MovieMetadata.objects.get(movie=instance)
    except:
        pass
    finally:
        if movie_metadata is None:
            movie_metadata = MovieMetadata(movie=instance)
            movie_metadata.save()
        else:
            pass


@receiver(post_save, sender=Movie)
def create_movie_dash_files(sender, instance, **kwargs):
    """ Trigger body """

    data = os.path.splitext(instance.video_file.name)
    name = data[0]
    ext = data[-1]

    if ext == ".mpd":
        return

    input_filename = os.path.join(settings.MEDIA_ROOT, instance.video_file.name)
    input_directory = os.path.dirname(input_filename)

    output_filename = os.path.join(settings.MEDIA_ROOT, "%s.mpd" % name)

    rep_360 = Representation(width=640, height=360, kilo_bitrate=276)
    rep_480 = Representation(width=854, height=480, kilo_bitrate=750)
    (
        ffmpeg_streaming.dash(
            input_filename, adaption='"id=0,streams=v id=1,streams=a"'
        )
        #        .format("libvpx-vp9")
        .format("libx264")
        .add_rep(rep_360, rep_480)
        .package(output_filename)
    )

    # could lead to arbitrary code execution?
    command = "ffprobe -loglevel error -show_streams {0} | grep duration | cut -f2 -d= | tail -n1 | cut -f1 -d.".format(
        input_filename
    )
    command_output = os.popen(command).read()
    try:
        video_length = int(command_output)
    except:
        video_length = 0

    os.remove(input_filename)
    instance.video_file = File(open(output_filename))
    instance.length = video_length
    instance.save()

    output_filename = os.path.join(settings.MEDIA_ROOT, instance.video_file.name)
    output_directory = os.path.dirname(output_filename)
    files = os.listdir(input_directory)
    for f in files:
        try:
            shutil.move(os.path.join(input_directory, f), output_directory)
        except:
            pass
    shutil.rmtree(input_directory)


post_save.connect(create_movie_dash_files, sender=Movie)
post_save.connect(create_movie_metadata, sender=Movie)
pre_delete.connect(delete_movie, sender=MovieMetadata)
pre_delete.connect(delete_video_file_and_metadata, sender=Movie)


class Rating(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    movie = models.ForeignKey(Movie, on_delete=models.CASCADE)
    rating = models.FloatField(default=0.0)
    created_at = models.DateTimeField(default=now, editable=False)
    updated_at = models.DateTimeField(default=now, editable=False)

    # there's no way to make composite primary keys
    # this is a work around
    class Meta:
        unique_together = (("user", "movie"),)


class Review(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    movie = models.ForeignKey(Movie, on_delete=models.CASCADE)
    text = models.CharField(max_length=1024)
    created_at = models.DateTimeField(default=now, editable=False)
    updated_at = models.DateTimeField(default=now, editable=False)

    # there's no way to make composite primary keys
    # this is a work around
    class Meta:
        unique_together = (("user", "movie"),)


class Favorite(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    movie = models.ForeignKey(Movie, on_delete=models.CASCADE)
    created_at = models.DateTimeField(default=now, editable=False)
    updated_at = models.DateTimeField(default=now, editable=False)

    # there's no way to make composite primary keys
    # this is a work around
    class Meta:
        unique_together = (("user", "movie"),)


class History(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    movie = models.ForeignKey(Movie, on_delete=models.CASCADE)
    seconds = models.IntegerField(default=0)
    created_at = models.DateTimeField(default=now, editable=False)
    updated_at = models.DateTimeField(default=now, editable=False)

    # there's no way to make composite primary keys
    # this is a work around
    class Meta:
        unique_together = (("user", "movie"),)


class UserDetails(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)
    name = models.CharField(max_length=32, default="Anonymous")
    last_name = models.CharField(max_length=32, default="")
    address = models.CharField(max_length=128, default="No address")
    phone = models.CharField(max_length=10, default="No phone")
    created_at = models.DateTimeField(default=now, editable=False)
    updated_at = models.DateTimeField(default=now, editable=False)


class Suspension(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    start_date = models.DateTimeField(default=now, blank=True)
    end_date = models.DateTimeField()
    complete = models.BooleanField(default=False)
    description = models.CharField(max_length=32, default="No description")
    created_at = models.DateTimeField(default=now, editable=False)
    updated_at = models.DateTimeField(default=now, editable=False)

    def __str__(self):
        return "Suspension Id: %s | User Id: %s | Username: %s" % (
            self.id,
            self.user.id,
            self.user.username,
        )
