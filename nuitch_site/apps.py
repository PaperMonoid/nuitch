from django.apps import AppConfig


class NuitchSiteConfig(AppConfig):
    name = "nuitch_site"
