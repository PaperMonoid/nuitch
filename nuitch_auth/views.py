from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User, Group
from datetime import datetime, date
from .forms import LoginForm, SignupForm
from nuitch_site import urls
from nuitch_site.models import Suspension

# Create your views here.
def Login(request):
    if request.user.is_authenticated:
        return redirect("home_page")

    message = "Not Login"
    form = LoginForm()

    if request.method == "POST":
        form = LoginForm(request.POST or None)
        if form.is_valid():
            username = request.POST["username"]
            password = request.POST["password"]
            user = authenticate(request, username=username, password=password)
            if user is not None:
                if user.is_active:
                    suspensions = Suspension.objects.filter(user=user).filter(complete=False)
                    now = date.today()
                    suspended = False
                    for suspension in suspensions:
                        if now > suspension.start_date.date() and now < suspension.end_date.date():
                            suspended = True
                            message = "Your account has been suspended from {} to {}.".format(suspension.start_date.date(), suspension.end_date.date())

                        if now < suspension.start_date.date() or now > suspension.end_date.date():
                            suspension.complete = True
                            suspension.save()

                    if not suspended:
                        login(request, user)
                        message = "User logged"
                        return redirect("home_page")
                else:
                    message = "User is not active"
            else:
                message = "Username or password is not correct"
        else:
            message = "Invalid form"
    context = {"form": form, "message": message}
    return render(request, "nuitch_auth/login.html", context)


def Logout(request):
    logout(request)
    return redirect("login")


def Signup(request):
    if request.user.is_authenticated:
        return redirect("home_page")

    message = ""
    form = SignupForm()
    if request.method == "POST":
        form = SignupForm(request.POST or None)
        password = request.POST["password"]
        confirm_password = request.POST["confirm_password"]
        if password == confirm_password:
            if form.is_valid():
                user = User.objects.create_user(
                    username=request.POST["email"], email=request.POST["email"]
                )
                user.set_password(password)
                # add user to users group
                user_group = Group.objects.get(name="User")
                user.groups.add(user_group)
                # user is not staff
                user.is_staff = False
                # user is not superuser
                user.is_superuser = False
                # user is active
                user.is_active = True
                # save
                user.save()
                # login after signup
                user = authenticate(
                    request, username=request.POST["email"], password=password
                )
                if user is not None:
                    if user.is_active:
                        login(request, user)
                        return redirect("home_page")
                    else:
                        message = "User is not active"
                else:
                    message = "User not signup correctly"

            else:
                message = "Invalid form"
        else:
            message = "Passwords are not equals"
    context = {"form": form, "message": message}
    return render(request, "nuitch_auth/signup.html", context)
