from django.apps import AppConfig


class NuitchAuthConfig(AppConfig):
    name = 'nuitch_auth'
